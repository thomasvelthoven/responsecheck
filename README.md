# utility to check if expires and max-age properties match

## build:

- java8
- mvn package
	

## usage:

	java -jar target/responsecheck-1.0-SNAPSHOT-jar-with-dependencies.jar <url1> [<url2> ...]