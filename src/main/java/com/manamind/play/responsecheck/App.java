package com.manamind.play.responsecheck;

import java.util.List;

import com.google.inject.Key;
import com.google.inject.name.Names;
import joptsimple.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class App
{
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    Injector injector;

    public static void main( String[] args ) throws Exception {
        ResponseAnalyzer ra = new ResponseAnalyzer();

        for (String url : args) {
            LOGGER.info(url);
            ra.test(url);
        }



    }
    public App() {
        injector =
            Guice.createInjector(
                new AbstractModule(){
                    @Override
                    protected void configure() {
                        /*bind(EventLogger.class).to(EventLoggerImpl.class);
                        bind(MessageListener.class).annotatedWith(Names.named("TCP")).to(MessageListenerTCP.class);
                        bind(MessageSender.class).annotatedWith(Names.named("TCP")).to(MessageSenderTCP.class);
                        bind(MessageListener.class).annotatedWith(Names.named("UDP")).to(MessageListenerUDP.class);
                        bind(MessageSender.class).annotatedWith(Names.named("UDP")).to(MessageSenderUDP.class);*/
                    }
                });
    }
}
