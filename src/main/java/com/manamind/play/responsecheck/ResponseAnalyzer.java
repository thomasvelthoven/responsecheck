package com.manamind.play.responsecheck;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import javax.swing.text.html.Option;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.DateUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

/**
 * @author Manamind AS
 */
public class ResponseAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseAnalyzer.class);

    void test(String url) throws Exception {

        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
            builder.build());
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(
            sslsf).build();

        /*CloseableHttpClient httpClient = HttpClients.createDefault();*/

        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("User-Agent", ResponseAnalyzer.class.getSimpleName());
        httpGet.setHeader("Accept", "*/*");

        CloseableHttpResponse response = httpClient.execute(httpGet);

        Optional<Date> date = getDate(response, HttpHeaders.DATE);

        Optional<String> maxAge = getCacheControlField(response, "max-age");
        Optional<Long> maxAgeValue = getCacheControlFieldValueLong(maxAge);

        Optional<Long> age = getLong(response, HttpHeaders.AGE);

        long msDateMinusAge;
        if (age.isPresent()) {
            msDateMinusAge = date.get().getTime() - (age.get() * 1000);
            LOGGER.info("{} = {} - {} (date - age)", formatDate(Optional.of(new Date(msDateMinusAge)), "CET"),
                formatDate(date, "CET"), age.get());
        } else {
            msDateMinusAge=0;
            LOGGER.info("no (age)");
        }

        Optional<Date> expires = getDate(response, HttpHeaders.EXPIRES);
        if (maxAgeValue.isPresent()) {
            long msDatePlusMaxAge = date.get().getTime() + (maxAgeValue.get() * 1000);
            long msAgeCompensatedDatePlusMaxAge = (date.get().getTime() - msDateMinusAge) + (maxAgeValue.get() * 1000);
            Date dateDatePlusMaxAge = new Date(msAgeCompensatedDatePlusMaxAge);
            Date dateAgeCompensatedDatePlusMaxAge = new Date(msDatePlusMaxAge);
            LOGGER.info("{} = {} + {} (date + maxAgeValue)",
                formatDate(Optional.of(new Date(msDatePlusMaxAge)), "CET"),
                formatDate(date, "CET"), maxAgeValue.get());

            LOGGER.info("{} = {} + {} (ageCompensatedDate + maxAgeValue)",
                formatDate(Optional.of(new Date(msAgeCompensatedDatePlusMaxAge)), "CET"),
                formatDate(date, "CET"), maxAgeValue.get());

            if (expires.isPresent()) {
                LOGGER.info("expires diff: {}", Math.abs(dateDatePlusMaxAge.getTime() - expires.get().getTime()) /1000);
                LOGGER.info("expires diff (age compensated): {}",
                    Math.abs(dateAgeCompensatedDatePlusMaxAge.getTime() - expires.get().getTime()) /1000);
            }
        }
        Optional<Date> lastModified = getDate(response, HttpHeaders.LAST_MODIFIED);


    }

    private Optional<Long> getCacheControlFieldValueLong(Optional<String> cacheControlField) {
        if(cacheControlField.isPresent()) {
            int index = cacheControlField.get().indexOf('=');
            if (index == -1) {
                return Optional.empty();
            }
            String fieldName = cacheControlField.get().substring(0, index).trim();
            Long fieldValue = Long.parseLong(cacheControlField.get().substring(index+1));

            LOGGER.info("{} ({})", fieldValue, fieldName);
            return Optional.of(fieldValue);
        } else {
            return Optional.empty();
        }
    }

    private Optional<String> getCacheControlField(CloseableHttpResponse response, String cacheControlFieldName) throws Exception {
        Optional<Header> cacheControlHeader = getHeader(response, "Cache-Control");
        if (!cacheControlHeader.isPresent()) {
            cacheControlHeader = getHeader(response, "cache-control");
        }
        if (cacheControlHeader.isPresent()) {
            String[] cacheControlHeaderFields = cacheControlHeader.get().getValue().split(",");
            for (String cacheControlHeaderField : cacheControlHeaderFields) {
                if (cacheControlHeaderField.trim().startsWith(cacheControlFieldName)) {
                    return Optional.of(cacheControlHeaderField);
                }
            }
            return Optional.empty();
        } else {
            return Optional.empty();
        }
    }

    void unitTest(String url) throws Exception {
        WebConversation wc = new WebConversation();
        WebRequest req = new GetMethodWebRequest( url );
        WebResponse response = wc.getResponse( req );

        Optional<Date> date = getDate(response, HttpHeaders.DATE);
        Optional<Date> lastModified = getDate(response, HttpHeaders.LAST_MODIFIED);
        Optional<Date> expires = getDate(response, HttpHeaders.EXPIRES);
        Optional<Long> age = getLong(response, HttpHeaders.AGE);


    }

    private Optional<Date> getDate(WebResponse response, String headerName) throws Exception {
        String headerValue = response.getHeaderField(headerName);
        if (headerValue == null) {
            return Optional.empty();
        } else {
            return getDate(Optional.of(headerName), Optional.of(headerValue));
        }
    }
    private Optional<Long> getLong(WebResponse response, String headerName) throws Exception {
        String headerValue = response.getHeaderField(headerName);
        if (headerValue == null) {
            return Optional.empty();
        } else {
            return getLong(Optional.of(headerName), Optional.of(headerValue));
        }
    }


    private Optional<Long> getLong(CloseableHttpResponse response, String headerName) throws Exception {
        Optional<Header> header = getHeader(response, headerName);
        return getLong(header);
    }

    private Optional<Long> getLong(Optional<Header> header) {

        if (header.isPresent()) {
            return getLong(Optional.of(header.get().getName()), Optional.of(header.get().getValue()));
        } else {
            return Optional.empty();
        }
    }
    private Optional<Long> getLong(Optional<String> headerName, Optional<String> headerValue) {
        if (headerValue.isPresent()) {
            Optional<Long> value = Optional.of(Long.parseLong(headerValue.get()));
            LOGGER.info("{} ({})",
                value.get(), headerName.get());
            return value;
        } else {
            return Optional.empty();
        }
    }

    private Optional<Date> getDate(CloseableHttpResponse response, String headerName) throws Exception {
        Optional<Header> header = getHeader(response, headerName);
        return getDate(header);
    }

    private Optional<Date> getDate(Optional<Header> header) {
        if (header.isPresent()) {
            return getDate(Optional.of(header.get().getName()), Optional.of(header.get().getValue()));
        } else {
            return Optional.empty();
        }

    }
    private Optional<Date> getDate(Optional<String> headerName, Optional<String> headerValue) {
        if (headerValue.isPresent()) {
            Optional<Date> date = Optional.of(DateUtils.parseDate(headerValue.get()));
            String fieldName = headerName.get();
            logDateField(fieldName, date);
            return date;
        } else {
            return Optional.empty();
        }
    }

    private void logDateField(String fieldName, Optional<Date> date) {
        String timeZoneSymbol = "CET";
        String formattedDate = formatDate(date, timeZoneSymbol);
        LOGGER.info("{} ({})",
            formattedDate, fieldName);
    }

    private String formatDate(Optional<Date> date, String timeZoneSymbol) {
        TimeZone tz = TimeZone.getTimeZone(timeZoneSymbol);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(tz);
        return dateFormat.format(date.get()) + ", " +
        tz.getDisplayName(false, TimeZone.SHORT);
    }

    private Optional<Header> getHeader(CloseableHttpResponse response, String s) throws Exception {
        Header[] headers = response.getHeaders(s);
        if (headers == null || headers.length == 0) {
            return Optional.empty();
        } else if (headers.length > 1) {
            throw new Exception("unexpected multiple " + s + " responses");
        } else {
            return Optional.of(headers[0]);
        }
    }
}
